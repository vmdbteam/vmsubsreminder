CREATE DATABASE IF NOT EXISTS `vmsubsreminder`;

USE `vmsubsreminder`;

CREATE TABLE IF NOT EXISTS `message` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message_text` VARCHAR(160) NOT NULL,
  `club_id` INT DEFAULT NULL,
  `service_id` INT NOT NULL,
  `message_category` ENUM('OLD', 'NEW') NOT NULL DEFAULT 'NEW',
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_club_service` (`club_id`, `service_id`, `message_category`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `audit_log` (
    `id`                INT                 NOT NULL    AUTO_INCREMENT,
    `message_id`        INT                 NOT NULL,
    `msisdn`            VARCHAR(21)         NOT NULL,
    `transaction_id`    VARCHAR(40)         NULL,
    `created`           TIMESTAMP           NOT NULL    DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `ix_msisdn` (`msisdn`),
  INDEX `ik_message_id` (`message_id`),
  CONSTRAINT `fk_audit_log_message`
    FOREIGN KEY (`message_id`)
    REFERENCES `message` (`id`)
)
ENGINE = InnoDB;

-- move data from `vmreminder`.`ReminderClub` to `vmsubsreminder`.`messages`
INSERT INTO `message`
    (`service_id`, `club_id`, `message_category`, `message_text`)
SELECT `ServiceId`, `ClubId`, 'NEW', `ClubMessage`
FROM `vmreminder`.`ReminderClub`;

-- move data from `vmreminder`.`ReminderService` to `vmsubsreminder`.`messages`
INSERT INTO `message`
    (`service_id`, `club_id`, `message_category`, `message_text`)
SELECT ServiceId, NULL, 'NEW', ServiceMessage
FROM `vmreminder`.`ReminderService`;

-- add OLD messages as well
INSERT INTO `message`
    (`service_id`, `club_id`, `message_category`, `message_text`)
SELECT ServiceId, NULL, 'OLD', ServiceMessage
FROM `vmreminder`.`ReminderService`;

