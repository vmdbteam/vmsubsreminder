-- Test DB patch to create subs and reminder messages that will be sent out in order to test all combinations business requires.
/* Test requiremetns:
Before - 2009-10-01:
• No reminder messages

Before - 2013-03-01:
• One reminder message per SERVICE only
• Reminder, ur a member of the #sc# service. Cost R#price#/#freqword#. To unsub go to *120*#sc## CC 0861111106 (VAS)

From - 2013-03-01:
• One reminder message per CLUB only
• Reminder, ur a member of a #club# club. Cost R#price#/#freqword#. To unsub go to *120*#sc## CC 0861111106 (VAS)

For all:
• Reminders to be sent on monthly anniversary date
• Send message only if subscriber has had a successful bill in past 7 days
• ** #price#:

    • Must be converted from decicents (3000 -> 3)
    • If the decimal is 00 then fall away (3.00 -> 3)
    • If decimal is not 00, then it must have 2 characters (so will work for R0.50 and R0.05) (500 decicents -> 0,50; 50 decicents -> 0.05)
*/

/* Test criteria
a)	1 subscription on 1 service
	a.	Join date 2005
	b.	Join date 2011
	c.	Join date 2015
b)	2 subscriptions on 1 service
	a.	Join date 2005
	b.	Join date 2011
	c.	Join date 2015
c)	2 subscriptions on 2 different services EACH
	a.	Join date 2005
	b.	Join date 2011
	c.	Join date 2015
d)	3 subscriptions across 2 different services
	a.	Join date 2005
	b.	Join date 2011
	c.	Join date 2015
e)	2 subscriptions on 1 service, handling of First day Free. Change JOIN price to be different to RENEW price.
	a.	Join date 2005
	b.	Join date 2011
	c.	Join date 2015
f)	1 subscription on 1 service, subs marked as externally renewed (RMCS)
	a.	Join date 2005
	b.	Join date 2011
	c.	Join date 2015

Clubs to be used: 
a)	1707 (R3, XL)
b)	1640 (R1, SX18)
	1568 (R5, SX18)
c)	986 (R3, XL)
	1901 (R5, XL)
	899 (R2, Mobi888)
	2039 (R5, Mobi888)
d)	2015 (R5, 18plus)
	2290 (R3, XL)
	2289 (R3, XL) --> Updated reminder message with club override message
e)	2269 (R3, Mobi888)
	994 (R2, Mobi888)
*/

-- SQL to lookup related details
/*select * from vmsubsreminder.message;
select * from vmsubsreminder.message where 
club_id in (
	1707, 
    1640, 1568,
    986, 1901, 899, 2039,
    2015, 2290, 2289,
    2269, 994
)
or service_id in (58, 33, 37, 135, 147);
select distinct id, serviceId from vmsubs.substype where id in (
	1707, 
    1640, 1568,
    986, 1901, 899, 2039,
    2015, 2290, 2289,
    2269, 994
);
select * from vmmap.Services where ServiceId in (58, 33, 37, 135, 147);

select * from vmmap.Services where ServiceId in (58, 33, 37, 135, 147, 98);
select Id, SubsName, BillAmount, RenewAmount, ExpiryDays, ShortCode from vmsubs.substype st
left join vmmap.ShortCodes sc on st.ServiceId = sc.ServiceId and sc.IsDefault=1
where Id in (
	1707, 
    1640, 1568,
    986, 1901, 899, 2039,
    2015, 2290, 2289,
    2269, 994
);
select * from vmmap.ShortCodes;
select * from vmsubs.subscriptions
where CellNumber in (
 '27820000001','27820000002','27820000003','27820000003','27820000004','27820000005','27820000006','27820000007',
 '27820000008','27820000009','27820000010','27820000011','27820000012','27820000013','27820000014','27820000015',
 '27820000016','27820000017', '27820000018'
);
select * from vmsubs.subscriptions_head
where msisdn in (
 '27820000001','27820000002','27820000003','27820000003','27820000004','27820000005','27820000006','27820000007',
 '27820000008','27820000009','27820000010','27820000011','27820000012','27820000013','27820000014','27820000015',
 '27820000016','27820000017', '27820000018'
);
*/

-- Created/Update messages
-- OLD service level Text
UPDATE vmsubsreminder.message 
SET `message_text` = 'Reminder, ur a member of the #sc# service. Cost R#price#/#freqword#. To unsub go to *120*#sc## CC 0861111106 (VAS)', 
updated = NOW() 
WHERE service_id IN (58, 33, 37, 135) and message_category = 'OLD';
-- NEW service level text
UPDATE vmsubsreminder.message 
SET `message_text` = 'Reminder, ur a member of a #pkg_name# club. Cost R#price#/#freqword#. To unsub go to *120*#sc## CC 0861111106 (VAS)', 
updated = NOW() 
WHERE service_id IN (58, 33, 37, 135) and message_category = 'NEW';

-- Add club level override message for club 2289
DELETE FROM vmsubsreminder.message WHERE club_id IN (2289);
INSERT INTO vmsubsreminder.message 
(`message_text`, club_id, service_id, message_category, created) 
VALUES 
('Reminder, ur a member of the #sc# service. Cost R#price#/#freqword#. To unsub send STOP #pkg_name# to #sc# CC 0861111106 (VAS)', 2289, 37, 'OLD', NOW()),
('Reminder, ur a member of a #pkg_name# club. Cost R#price#/#freqword#. To unsub send STOP #pkg_name# to #sc# CC 0861111106 (VAS)', 2289, 37, 'NEW', NOW());

-- Add new message for RMCS, should not be required but want to test this.
DELETE FROM vmsubsreminder.message WHERE service_id IN (147);
INSERT INTO vmsubsreminder.message 
(`message_text`, service_id, message_category, created) 
VALUES 
('Reminder, ur a member of the #pkg_name# club. Cost R#price#/#freqword#. To unsub contact Jet call center.', 147, 'NEW', NOW());



-- Create subscriptions
-- A. 
-- 1 sub on 1 service with join dates:
-- 2005 - 27820000001
-- 2011 - 27820000002
-- 2015 - 27820000003

-- 2005 - 27820000001
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000001';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000001');
SET @CLUBID = 1707;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE );
select @CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE;
select * from vmsubs.subscriptions_head order by id desc limit 20;
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
@SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
2
FROM vmsubs.substype st
WHERE st.Id = @CLUBID;
-- 2011 - 27820000002
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000002';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000002');
SET @CLUBID = 1707;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);

-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000003
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000003';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000003');
SET @CLUBID = 1707;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);

-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;


-- B. 
-- 2 subs on 1 service with join dates:
-- 2005 - 27820000004
-- 2011 - 27820000005
-- 2015 - 27820000006

-- 2005 - 27820000004 1st sub 1640
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000004';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000004');
SET @CLUBID = 1640;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000004 2nd sub 1568
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000004';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000004');
SET @CLUBID = 1568;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2011 - 27820000005 1st sub 1640
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000005';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000005');
SET @CLUBID = 1640;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000005 2nd sub 1568
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000005';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000005');
SET @CLUBID = 1568;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000006 1st sub 1640
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000006';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000006');
SET @CLUBID = 1640;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000006 2nd sub 1568
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000006';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000006');
SET @CLUBID = 1568;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);

-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;


-- C. 
-- 2 subs on 2 services each with join dates:
-- 2005 - 27820000007
-- 2011 - 27820000008
-- 2015 - 27820000009

-- 2005 - 27820000007 1st sub 986
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000007';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000007');
SET @CLUBID = 986;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000007 2nd sub 1901
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000007';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000007');
SET @CLUBID = 1901;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000007 3rd sub 899
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000007';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000007');
SET @CLUBID = 899;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000007 4th sub 2039
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000007';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000007');
SET @CLUBID = 2039;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2011 - 27820000008 1st sub 986
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000008';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000008');
SET @CLUBID = 986;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000008 2nd sub 1901
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000008';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000008');
SET @CLUBID = 1901;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000008 3rd sub 899
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000008';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000008');
SET @CLUBID = 899;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000008 4th sub 2039
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000008';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000008');
SET @CLUBID = 2039;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2015 - 27820000008 1st sub 986
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000009';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000009');
SET @CLUBID = 986;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000008 2nd sub 1901
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000009';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000009');
SET @CLUBID = 1901;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000008 3rd sub 899
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000009';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000009');
SET @CLUBID = 899;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000008 4th sub 2039
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000009';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000009');
SET @CLUBID = 2039;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- D. 
-- 3 subs on 2 services each with join dates:
-- 2005 - 27820000010
-- 2011 - 27820000011
-- 2015 - 27820000012

-- 2005 - 27820000010 1st sub 2015
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000010';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000010');
SET @CLUBID = 2015;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000010 2nd sub 2290
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000010';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000010');
SET @CLUBID = 2290;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000010 3rd sub 2289
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000010';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000010');
SET @CLUBID = 2289;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2011 - 27820000011 1st sub 2015
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000011';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000011');
SET @CLUBID = 2015;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000011 2nd sub 2290
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000011';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000011');
SET @CLUBID = 2290;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000011 3rd sub 2289
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000011';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000011');
SET @CLUBID = 2289;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2015 - 27820000012 1st sub 2015
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000012';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000012');
SET @CLUBID = 2015;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000012 2nd sub 2290
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000012';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000012');
SET @CLUBID = 2290;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000012 3rd sub 2289
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000012';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000012');
SET @CLUBID = 2289;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;


-- E. 
-- 2 subs on 1 service with different join price to the renew price each with join dates:
-- 2005 - 27820000013
-- 2011 - 27820000014
-- 2015 - 27820000015

-- 2005 - 27820000013 1st sub 2269
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000013';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000013');
SET @CLUBID = 2269;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, 1, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2005 - 27820000013 2nd sub 994
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000013';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000013');
SET @CLUBID = 994;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, 1, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2011 - 27820000014 1st sub 2269 - weekly renew with different join and renew amount
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000014';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000014');
SET @CLUBID = 2269;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, 500, 1, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), 7, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000014 2nd sub 994 - weekly renew with different join and renew amount

SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000014';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000014');
SET @CLUBID = 994;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, 50, 1, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), 7, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- 2015 - 27820000015 1st sub 2269 - monthly renew with different join and renew amount
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000015';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000015');
SET @CLUBID = 2269;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, 3050, 1, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), 30, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000015 2nd sub 994 - monthly renew with different join and renew amount
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000015';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000015');
SET @CLUBID = 994;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, 3500, 1, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), 30, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    2
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;

-- F. 
-- 1 subs on 1 service, subs marked as externally renewed (RMCS) each with join dates:
-- 2005 - 27820000016
-- 2011 - 27820000017
-- 2015 - 27820000018
-- select * from vmmap.BillingChannels;

-- 2005 - 27820000016 1st sub 2136
SET @SUBDATE = CONCAT('2005-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000016';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000016');
SET @CLUBID = 2136;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    3
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2011 - 27820000017 1st sub 2136
SET @SUBDATE = CONCAT('2011-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000017';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000017');
SET @CLUBID = 2136;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    3
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;
-- 2015 - 27820000018 1st sub 2136
SET @SUBDATE = CONCAT('2015-', MONTH(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH)), '-', DAY(LAST_DAY(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH))), ' 10:00:00');
SET @MSISDN = '27820000018';
SET @CUSTID = (SELECT Id FROM vmcim.Customers WHERE MobileNumber = '27820000018');
SET @CLUBID = 2136;
-- Remove and create subscription_head records
DELETE FROM vmsubs.subscriptions_head WHERE msisdn = @MSISDN AND substypeid = @CLUBID;
INSERT INTO vmsubs.subscriptions_head
(customerid, msisdn, substypeid, startcount, datecreated, dateupdated)
VALUES
(@CUSTID, @MSISDN, @CLUBID, 1, @SUBDATE, @SUBDATE);
-- Remove and create subscription records.
DELETE FROM vmsubs.subscriptions WHERE CellNumber = @MSISDN AND substypeid = @CLUBID;
INSERT INTO `vmsubs`.`subscriptions`
(`CellNumber`,`SubsTypeId`,`DateCreated`,`Active`,`Parked`,`BillCount`,`CreditsPerBill`,`BillAmount`,`JoinAmount`,
`DateRestarted`,`DateStopped`,`DateParked`,`DateUnParked`,`ClientId`,`Account`,`SenderAddrId`,`NextBillAttempt`,
`ExpiryDate`,`ExpiryDays`,`FreeExpiryDate`,`ServiceName`,`Pending`,`Status`,`AutoSub`,`AutoStop`,`RenewRetryInd`,
`Temporary`,`ConversionDate`,`ActivateSource`,`StopSource`,`CreateSource`,`RestartSource`,`ActivationDate`,`LastRenewDate`,
`NetworkId`,`Balance`,`Shifted`,`ShiftedDate`,`IsPush`,`SubsEventId`,`PushBatchSerial`,`DateUpdated`,`AuthRef`,`DoiToken`,
`BillChannelId`)
SELECT @MSISDN, st.Id, @SUBDATE, 1, 0, 10, st.CreditsPerBill, st.RenewAmount, st.BillAmount, 
    @SUBDATE, '1899-12-30 00:00:00', '1899-12-30 00:00:00', '1899-12-30 00:00:00', st.ClientId, '', 0, DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 1 DAY), st.ExpiryDays, @SUBDATE, st.ServiceName, 0, 0, 0, 0, 1,
    0, @SUBDATE, 2, 0, 0, 0, @SUBDATE, DATE_SUB(NOW(), INTERVAL 1 DAY),
    1, st.CreditsPerBill, 0, '1899-12-30 00:00:00', 0, 0, 0, DATE_SUB(NOW(), INTERVAL 1 DAY), 'VIAMEDIATEST', 'VIAMEDIATEST', 
    3
	FROM vmsubs.substype st
    WHERE st.Id = @CLUBID;


-- Current reminder sql:
/*
SELECT 
	s.JoinAmount AS join_amount,
	s.cellnumber AS cell_number, 
	st.renewamount AS bill_amount, 
	st.expirydays AS expiry_days, 
	st.serviceId AS service_id, 
	s.substypeid AS club_id, 
	st.clientId AS client_id, 
	s.daterestarted AS date_restarted, 
	st.SubsName AS subscription_name, 
	sc.ShortCode AS short_code,
CASE 
	WHEN s.daterestarted >= '2013-03-01 00:00:00' 
	THEN 1 
	ELSE 0 
END AS service_new 
FROM 
	vmsubs.subscriptions s 
JOIN vmsubs.substype st ON st.id = s.substypeid 
JOIN vmmap.ShortCodes sc ON (st.ServiceId = sc.ServiceId AND sc.IsActive = 1 AND sc.isDefault = 1) 
JOIN vmmap.BillingChannels bc ON (s.BillChannelId = bc.BillChannelId AND bc.ExternallyRenewed = 0) 
WHERE 
	s.active = 1 AND s.Temporary = 0 
	AND s.Parked = 0 
	AND s.Pending = 0 
	AND s.NetworkId IN (1, 2, 3, 4, 5)
	AND DATE_ADD(s.lastrenewdate, INTERVAL 15 DAY) > CONCAT(CURRENT_DATE, ' ', '09:30:00') 
	AND s.daterestarted >= '2011-10-01 00:00:00' 
	AND s.Billcount > 0 
	AND DATE_ADD(DATE(s.dateRestarted), INTERVAL 27 DAY) < CONCAT(CURRENT_DATE, ' ', '09:30:00') 
	AND 
	(
		DAY(s.daterestarted) = DAY(CONCAT(CURRENT_DATE, ' ', '09:30:00')) 
		OR DAY(s.daterestarted) > LAST_DAY(CONCAT(CURRENT_DATE, ' ', '09:30:00'))
	)
	AND s.cellnumber NOT IN (SELECT audit_log.msisdn FROM vmsubsreminder.audit_log WHERE DATE(audit_log.created) = CURRENT_DATE);
*/

/* Possible issue if user subscribed on the 31st and the current month only has 30 days
DAY(s.daterestarted) = DAY(CONCAT(CURRENT_DATE, ' ', '09:30:00')) 
OR DAY(s.daterestarted) > LAST_DAY(CONCAT(CURRENT_DATE, ' ', '09:30:00'))
e.g.
select DAY(CONCAT('2017-07-31', ' ', '09:30:00')), LAST_DAY(CONCAT('2017-09-28', ' ', '09:30:00')),
DAY('2017-07-31 09:30:00') > LAST_DAY(CONCAT('2017-09-28', ' ', '09:30:00'));
*/
/* change these to the subscription value
st.renewamount AS bill_amount, -> s.BillAmount
st.expirydays AS expiry_days, -> s.ExpiryDays
*/
